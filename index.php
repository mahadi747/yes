<?php require_once('demo/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Wchat is Premium Admin Dashboard Template with Flat design, Bootstrap Framework, Less, HTML5, CSS3 & Media Query. Collection of Lots of UI Components and more than thousand Pages">
    <meta name="author" content="">
    <meta name="keyword" content="metronic, admin templates, free dashboard templates, admin panel template, html dashboard templates, bootstrap themes, bootstrap template, bootstrap admin template, Wchat, metronic admin, free dashboard themes, premium dashboard template">
    <link rel="icon" type="image/png" sizes="16x16" href="demo/plugins/images/favicon.png">
    <title>Wchat is Premium Admin Dashboard Template with Flat design, Bootstrap Framework, Less, HTML5, CSS3 & Media Query.</title>
    <!-- Bootstrap Core CSS -->
    <link href="demo/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!--My admin Custom CSS -->
    <link href="demo/plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="demo/plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
<body class="">
<!-- Preloader -->

<div id="wrapper">
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12 navbar-fixed">
                <div class="fix-width">
                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo $config['site_url']; ?>"><img src="images/wchat.png" alt="Wchat" /></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                            <ul class="nav navbar-nav custom-nav navbar-right">
                                <li><a href="#myfeatures">FEATURES</a></li>
                                <li><a href="<?php echo $config['site_url']; ?>documentation.html" target="_blank">DOCUMENTATION</a></li>
                                <li><a href="#" class="btn btn-danger btn-rounded custom-btn" target="_blank">BUY NOW</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
        <!-- /Row -->



        <div class="row">
            <div class="col-md-12">
                <div class="fix-width text-center banner-part">
                    <h1 class="banner-title">The Ultimate Inbox Messaging Responsive Web App Kit</h1>
                    <span class="banner-small-text">Multipurpose Wchat Theme with <b>Light & Dark Versions</b>, Theme Modify, <b>Stylish Profile</b>, Smiley emoji, Integrated Plugins, Very Smooth, Font Icons, Image/file Upload</span>
                    <div class="btn-box">
                        <a href="<?php echo $config['site_url']; ?>demo/index.php" class="left-btn">VIEW ALL DEMOS</a>
                        <a href="#" target="_blank" class="right-btn"></a></div>
                    <img src="images/banner-img.jpg" class="img-responsive" />
                </div>
            </div>
        </div>
        <!-- Row -->

        <!-- .row -->
        <div class="row" id="choose-demo">
            <div class="col-lg-12">
                <div class="light-blue-bg">
                    <center>
                        <small class="text-megna">BEST PHP CHAT APP OF 2016 </small>
                        <h2 class="font-500">Simple & easy to customize, no other headaches</h2>
                        <p>Don’t go by our Words, checkout our awesome demos and verify yourself. <br/>Save 1000s of hours of designing and coding work as we already did that for you.</p>
                    </center>
                    <div class="fix-width demo-boxes">
                        <div class="row">

                            <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                <div class="white-box">
                                    <img src="images/screen1.jpg" class="img-responsive" />
                                    <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="<?php echo $config['site_url']; ?>demo" target="_blank">Live Preview</a></div>
                                </div>
                                <h5 class="m-t-20 font-500">Android/iphone</h5>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                <div class="white-box">
                                    <img src="images/screen2.jpg" class="img-responsive" />
                                    <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="<?php echo $config['site_url']; ?>demo" target="_blank">Live Preview</a></div>
                                </div>
                                <h5 class="m-t-20 font-500">Desktop/Laptop</h5>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                <div class="white-box">
                                    <img src="images/screen3.jpg" class="img-responsive" />
                                    <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="<?php echo $config['site_url']; ?>demo" target="_blank">Live Preview</a></div>
                                </div>
                                <h5 class="m-t-20 font-500">Tablet</h5>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 p-t-10 text-center demo-text">
                                <a class="btn btn-outline btn-rounded btn-default">More Coming Soon</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row m-t-60" id="myfeatures">
            <div class="col-md-12">
                <div class="fix-width">
                    <div class="row">
                        <div class="col-md-7 pull-right auto-img"><img src="images/responsive.jpg" /></div>
                        <div class="col-md-5 demo-text">
                            <small class="text-megna">LOTS OF ELEMENTS ADDED</small>
                            <h2 class="font-500">Responsive Web UI Kit for all your Needs</h2>
                            <p class="m-t-30 m-b-30">You will get almost everything you need. We have added all possible widgets to make your life easier with easy editing. Check them out.</p>
                            <a class="btn btn-outline btn-rounded btn-default" href="#myfeatures" target="_blank">Check all widgets</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row m-t-60">
            <div class="col-md-12">
                <div class="fix-width">
                    <div class="row">
                        <div class="col-md-7 auto-img "><img src="images/ecommerce.jpg" class="pull-right" /></div>
                        <div class="col-md-5 demo-text">
                            <small class="text-megna">LOTS OF ELEMENTS ADDED</small>
                            <h2 class="font-500">Theme Modify<br/>Option Included</h2>
                            <p class="m-t-30 m-b-30">Yes, We have also added Theme colors modify option. 6 Different colors for Your chat page.</p>

                            <a class="btn btn-outline btn-rounded btn-default" href="<?php echo $config['site_url']; ?>demo" target="_blank">Check in Demo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row m-t-60">
            <div class="col-md-12">
                <div class="fix-width">
                    <div class="row">
                        <div class="col-md-7 pull-right auto-img"><img src="images/inbox.jpg" /></div>
                        <div class="col-md-5 demo-text">
                            <small class="text-megna">LOTS OF ELEMENTS ADDED</small>
                            <h2 class="font-500">Cutest Smiley <br/>ready to use</h2>
                            <p class="m-t-30 m-b-30">Yup, Wchat Application is added Smiley widget. We have included 20+ Smileys, will be 100+ in next update.</p>
                            <a class="btn btn-outline btn-rounded btn-default" href="<?php echo $config['site_url']; ?>demo"  target="_blank">Check Smiley</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row m-t-60"></div>
        <!-- .row -->
        <div class="row m-t-60">
            <div class="col-md-12">
                <div class="fix-width">
                    <center class="col-md-6 col-md-offset-3">
                        <small class="text-megna">ALMOST COVERED EVERYTHING</small>
                        <h2 class="font-500">Other Amazing Features & <br/>Flexibility Provided</h2>
                    </center>

                </div>
            </div>

        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row inner-margin">
            <div class="fix-width">
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/color-skim.png"/>
                    <h4 class="font-500">6 Color Schemes</h4>
                    <p>We have included 6 pre-defined color schemes with Wchat.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/sidebars.png"/>
                    <h4 class="font-500">Dark & Light Sidebar</h4>
                    <p>Included Dark and Light Sidebar for getting desire look and feel.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/respo.png"/>
                    <h4 class="font-500">Fully Responsive</h4>
                    <p>All the layout of Wchat is Fully Responsive and widely tested.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/bootstraps.png"/>
                    <h4 class="font-500">Bootstrap 3x</h4>
                    <p>Its been made with Bootstrap 3x and full responsive layout.</p>
                </div>
                <!-- /.col -->
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row inner-margin">
            <div class="fix-width">
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/customized.png"/>
                    <h4 class="font-500">Easy to Customize</h4>
                    <p>Customization will be easy as we understand your pain.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/multi-upload.png"/>
                    <h4 class="font-500">Image/File Sending</h4>
                    <p>You can send Images, files to your frinds.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/less.png"/>
                    <h4 class="font-500">Sliding Contact List</h4>
                    <p>Responsive layout helps to more user friendly sliding contact bar.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-3 col-sm-6 text-center">
                    <img src="images/charts.png"/>
                    <h4 class="font-500">Smooth and fast</h4>
                    <p>All the layout of Wchat is Fully Smooth and fast.</p>
                </div>
                <!-- /.col -->
            </div>
        </div>
        <!-- /.row -->
       
        <!-- .row -->
        <div class="row inner-margin m-t-60">
            <div class="fix-width">
                <center><a href="#" target="_blank" class="btn btn-danger btn-rounded cst-btn">BUY Wchat NOW</a></center>
            </div>
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row m-t-60">
            <div class="col-md-12 dual-bg">
                <div class="fix-width">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 m-t-60">
                            <div class="p-20 p-b-0">
                                <small class="text-megna">LIGHT VERSION</small>
                                <h3 class="text-white font-500">Light Version of Wchat with <br/>Light & Dark Contact Sidebar Option</h3>
                                <img src="images/light-vrs.jpg" class="m-t-40 img-responsive" />
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 m-t-60">
                            <div class="p-20 p-b-0 text-right">
                                <small class="text-megna">DARK VERSION</small>
                                <h3 class="text-white font-500">Dark Version of Wchat with<br/>Light & Dark Contact Sidebar Option</h3>
                                <img src="images/drk-vrs.jpg" class="m-t-40 img-responsive" />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row m-t-60">
            <div class="col-md-12 m-t-40">
                <div class="fix-width">
                    <center class="col-md-6 col-md-offset-3">
                        <small class="text-megna">ALMOST COVERED EVERYTHING</small>
                        <h2 class="font-500">We are proud on Features provided with Wchat Template</h2>
                    </center>

                </div>
            </div>

        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row inner-margin">
            <div class="fix-width">
                <!-- .col -->
                <div class="col-md-4 col-sm-4 text-center">
                    <img src="images/docs.png"/>
                    <h4 class="font-500">Detailed Documentation</h4>
                    <p>We have made detailed documentation, <br/>so it will easy to use.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-4 col-sm-4 text-center">
                    <img src="images/dedicated.png"/>
                    <h4 class="font-500">Dedicated Support</h4>
                    <p>We believe in supreme support is <br/>key and we offer that.</p>
                </div>
                <!-- /.col -->
                <!-- .col -->
                <div class="col-md-4 col-sm-4 text-center">
                    <img src="images/ragular.png"/>
                    <h4 class="font-500">Regular Updates</h4>
                    <p>We are constantly updating our <br/>pack with new features.</p>
                </div>
                <!-- /.col -->

            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row m-t-40">
            <div class="fix-width">
                <center><a href="#" class="btn btn-danger btn-rounded cst-btn" target="_blank">BUY Wchat NOW</a></center>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row inner-margin">
            <div class="col-lg-12 m-t-40">
                &nbsp;
            </div>
        </div>
        <!-- /.row -->

        <!-- .row -->
        <footer class="footer">
            <div class="row">
                <div class="fix-width">
                    <div class="col-md-12 sub-footer">
                        <span>Copyright 2016. All Rights Reserved by <a class="text-white" href="#" target="_blank">Wchat</a></span>
                        <span class="pull-right">Design & Developed by <a class="text-white" href="#" target="_blank">Byweb.online</a></span>
                    </div>

                </div>
            </div>
        </footer>

        <!-- /.row -->

    </div>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="demo/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="demo/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- jQuery for carousel -->
<script src="demo/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
<script src="demo/assets/js/custom.js"></script>
</body>
</html>
